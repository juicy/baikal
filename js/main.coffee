$(document).ready(() ->
  tabs()
  header()
  parall()
)

parall = (slides) ->
  $('#place').parallax("50%", 400, 0.4, true);
  $('#rib_i').parallax("50%", 600, 0.3, true);
  $('#otd_i').parallax("50%", 1000, 0.2, true);
  $('#usl_i').parallax("50%", 1600, 0.1, true);
  $('#prices').parallax("50%", 600, 0.3, true);

header = () ->
  body = 'body'
  header = 'header'
  submenu = 'ul.submenu.with_str'

  check = () ->
    if ofs - $(window).scrollTop() <= 95
      $(submenu).addClass('fixed')
    else
      $(submenu).removeClass('fixed')

  if $(header).length > 0 && $(submenu).length > 0
    ofs = $(submenu).position().top
    check()
    $(window).scroll(() ->
      check()
    )
  else
    $(body).addClass('big_padding')



tabs = () ->
  labels = '.labels a.label'
  tabs = '.tabs .tab'

  to_a = (n) ->
    $(tabs).removeClass('active')
    $(labels).removeClass('active')
    $("#{labels}:eq(#{n})").addClass('active')
    $("#{tabs}:eq(#{n})").addClass('active')

  if $(labels).length > 0 && $(tabs).length > 0
    #if location.hash
    #  n = location.hash.replace('#', '')
    #  to_a(n)
    $(labels).click(() ->
      #location.hash = 
      n = $(this).parent().prevAll().length
      to_a(n)
      false
    )